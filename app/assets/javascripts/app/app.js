(function () {
	"use strict"
	angular.module("landing", [
		"ngResource", 
		"order",
		"ngMask"
	])
}());