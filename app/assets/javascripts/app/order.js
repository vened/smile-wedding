"use strict"

angular.module("order", [])

	.factory('Order', ['$http', function ($http) {
		return{
			create: function (obj) {
				return $http.post("/orders", obj);
			}
		}
	}])


	.controller('OrderFormCtrl', ['$scope', 'Order', function ($scope, Order) {

		$scope.order = {}
		$scope.order.name = ""
		$scope.order.phone = ""
		$scope.order.date = ""


		$scope.submit = function () {
			Order.create($scope.order).success(function (val) {
				if (val.error) {
					$scope.errors = val.error
				}
				if (val.success) {
					$scope.success = val.success
				}
			})
		}

	}]);
