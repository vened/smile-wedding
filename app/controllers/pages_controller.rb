class PagesController < ApplicationController

  # before_action :set_order, only: [:index, :show, :job]
  skip_before_filter :verify_authenticity_token, :only => [:order]


  def index
    @posts = Post.all
    @order = Order.new
    
    @carousels = Carousel.all
    @galleries = Gallery.all
    @videos = Video.all
    @reviews = Review.all
  end

  def order
    @order = Order.new(order_params)
    if @order.save
      OrderMailer.order_cofirm(@order).deliver
      render :json => {:success => @order}
    else
      render :json => {:error => @order.errors}
    end
  end


  private
  def order_params
    params.require(:page).permit(:name, :phone, :date)
  end


end
