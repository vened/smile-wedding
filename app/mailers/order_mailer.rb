# encoding: utf-8
class OrderMailer < ActionMailer::Base

  default_url_options[:host] = "http://smile-wedding.ru/"

  default :from => "order@smile-wedding.ru"

  def order_cofirm(order)
    @order = order
    mail(:to => "smile_weddings@mail.ru, maxstbn@yandex.ru", :subject => "Новая заявка http://smile-wedding.ru/")
  end

end
