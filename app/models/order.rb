class Order < ActiveRecord::Base

  validates :name, length: {minimum: 2}
  validates :phone, length: {minimum: 6}
end
